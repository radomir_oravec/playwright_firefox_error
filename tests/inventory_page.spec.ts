import { test} from '@playwright/test';

test.describe('Sample Suite', () => {
    
    test('Inventory Page - Navigate', async({page}) => {
        await page.goto('https://www.saucedemo.com/');
        await page.fill('#user-name', 'standard_user');
        await page.fill('#password', 'secret_sauce');
        await page.click('#login-button');
        await page.waitForLoadState('networkidle');
        await page.click('.shopping_cart_link');
        await page.waitForLoadState();
    })
})