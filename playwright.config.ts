import type { PlaywrightTestConfig } from '@playwright/test';
import { devices } from '@playwright/test';

const config: PlaywrightTestConfig = {
  testDir: './tests',
  reporter: process.env.CI ? [
    ['html', { open: 'never', outputFolder: './report' }],
    ['list'],
    ['junit', {outputFile: './report/junit.xml' }]
  ] : [
    ['html', { open: 'never', outputFolder: './report' }],
    ['list'],
  ],
  outputDir: 'output/',
  retries: 0,
  forbidOnly: !!process.env.CI,
  workers: process.env.CI ? 1 : undefined,
  reportSlowTests: { max: 0, threshold: 120000 },
  use: {
    trace: 'retain-on-failure',
    video: 'on'
  },
  projects: [
    {
      name: 'Desktop Chromium',
      use: {
        ...devices['Desktop Chrome'],
      },
    },
    {
      name: 'Desktop WebKit',
      use: {
        ...devices['Desktop Safari'],
      },
    },
    {
      name: 'Desktop Firefox',
      use: {
        ...devices['Desktop Firefox'],
      },
    },
  ],
};

export default config;
